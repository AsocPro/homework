# Gitlab
 - [ ] Create an account on GitLab
 - [ ] Create repository for this project
 - [ ] Add me (AsocPro) as a contributor to your repository
 - Commit code and push to GotLab repository
 - Focus on small sets of functionality for commits. Each small feature should be a separate commit.

# Address book

Create a simple address book application that lists a set of contacts. Each contact should have three parts: Namd, phone number and favorite color. The data for the contacts can be pretend and/or random data.
    
In the following sectiona of the app there are some items labeled (Advanced) Leave these items til the very last and finish all other objectives before starting on these.
   
   There are three basic views to this app:

## List view
 - The list view shouldshow a list of all the contacts names.
 - When an item is selected then the detail view should be presented.
 - (Advanced) There should be a search bar that eill allow for filtering of the dataFilter list view

## Detail view
 - Show all the details of the contact.
 - Show contact image. This can be the same photo for each person.

## Create new contact view
 - This is a screen that is used to create a new contact. It should have inputs for each of the fields of the contact. 
 - The new contacts do not have to be saved permanently. Just shown in the list.
 - (Advanced) Have a button that will open the camera to be able to attach a photo to the contact